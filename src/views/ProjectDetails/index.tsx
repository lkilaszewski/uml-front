import React, { useEffect, useState } from "react";
import Grid from "@material-ui/core/Grid";
import { useMutation, useQuery } from "react-query";
import { useParams } from "react-router";
import GmailTreeView from "../../components/ProjectDetails/ProjectTree";
import ProjectAppBar from "../../components/ProjectDetails/ProjectAppBar";
import ProjectBreadCrumbs from "../../components/ProjectDetails/ProjectBreadCrumbs";
import ProjectModalsManager from "../../components/Modals/ProjectModalsManager";
import RelationsTable from "../../components/DataDisplay/RelationsTable";
import Box from "@material-ui/core/Box";
import Button from "@material-ui/core/Button";
import { useStore } from "../../store";
import { Add } from "@material-ui/icons";
import { createStyles, Fab } from "@material-ui/core";
import NavigationIcon from "@material-ui/icons/Navigation";
import { makeStyles, Theme } from "@material-ui/core/styles";
import { green } from "@material-ui/core/colors";
import axiosInstance from "../../helpers/axios";

const createData = (
  name: string,
  calories: number,
  fat: number,
  carbs: number,
  protein: number
) => {
  return { name, calories, fat, carbs, protein };
};

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    absoluteButton: {
      position: "absolute",
      bottom: "15px",
      right: "15px",
    },
  })
);

const ProjectDetails = () => {
  const classes = useStyles();
  const { id } = useParams<{ id: string }>();
  console.log(id);
  const [_, actions] = useStore();
  const { isLoading, data } = useQuery(`/umlv/treeview/${id}/`);
  const [activeObject, setActiveObject] = useState<{ activeObject: any }>(null);

  useEffect(() => setActiveObject(data && data[0]), [isLoading]);
  if (isLoading) return <></>;
  return (
    <>
      <Grid container spacing={3}>
        <Grid item xs={12}>
          <ProjectBreadCrumbs />
        </Grid>
        <Grid item xs={3}>
          <ProjectAppBar />
          {!isLoading && (
            <GmailTreeView data={data} setActiveObject={setActiveObject} />
          )}
        </Grid>
        <Grid item xs={9}>
          <Box my={2}>
            <Grid
              container
              direction="row"
              justify="flex-end"
              alignItems="flex-start"
            >
              <Button
                variant="contained"
                color="primary"
                onClick={() => actions.openModal("addRelation", activeObject)}
                startIcon={<Add />}
              >
                Add New Relation
              </Button>
            </Grid>
          </Box>
          {activeObject && <RelationsTable activeObject={activeObject} />}
        </Grid>
      </Grid>
      <ProjectModalsManager />
      <div className={classes.absoluteButton}>
        <Button
          variant="contained"
          color="primary"
          onClick={() => actions.openModal("projectUML", id)}
          startIcon={<NavigationIcon />}
        >
          Generate Project UML
        </Button>
      </div>
    </>
  );
};
export default ProjectDetails;
