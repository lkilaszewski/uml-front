import React from "react";
import SignInForm from "../components/Authentication/SignInForm";

const SignIn = () => <SignInForm />;

export default SignIn;
