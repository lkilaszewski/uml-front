import React from "react";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import Box from "@material-ui/core/Box";
import { useQuery } from "react-query";
import { useStore } from "../../store";
import DataTable from "../../components/DataDisplay/DataTable";
import { Project } from "../../models";

const ProjectList = () => {
  const [_, actions] = useStore();
  const { isLoading, data } = useQuery("/umlv/project/");
  const projectData = data as Project[];
  return (
    <>
      <Box my={2}>
        <Grid
          container
          direction="row"
          justify="flex-end"
          alignItems="flex-start"
        >
          <Button
            variant="contained"
            color="primary"
            onClick={() => actions.openModal("addProject")}
          >
            Create New Project
          </Button>
        </Grid>
      </Box>
      {!isLoading && <DataTable rows={projectData} />}
    </>
  );
};

export default ProjectList;
