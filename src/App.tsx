import React from "react";
import { Router, Route, Switch } from "react-router-dom";
// eslint-disable-next-line import/no-extraneous-dependencies
import { createBrowserHistory } from "history";
import { QueryClient, QueryClientProvider } from "react-query";
import SignIn from "./views/SignIn";
import ProjectList from "./views/Projects";
import AuthenticatedLayout from "./components/Layouts/AuthenticatedLayout";
import ProjectDetails from "./views/ProjectDetails";
import { useStore } from "./store";
import axiosInstance from "./helpers/axios";

const defaultQueryFn = async ({ queryKey }) => {
  const { data } = await axiosInstance().get(`${queryKey[0]}`);
  return data;
};

const queryClient = new QueryClient({
  defaultOptions: {
    queries: {
      refetchOnWindowFocus: false,
      queryFn: defaultQueryFn,
    },
  },
});

const history = createBrowserHistory();

const App = () => {
  const [state] = useStore();
  return (
    <QueryClientProvider client={queryClient}>
      <Router history={history}>
        {state.token ? (
          <Switch>
            <Route
              path={"/dashboard"}
              render={({ match: { url } }) => (
                <AuthenticatedLayout>
                  <Route exact path={`${url}`} component={ProjectList} />
                  <Route
                    path={`${url}/project/:id`}
                    component={ProjectDetails}
                  />
                </AuthenticatedLayout>
              )}
            />
          </Switch>
        ) : (
          history.push("/")
        )}

        <Route path={"/"} exact component={SignIn} />
      </Router>
    </QueryClientProvider>
  );
};

export default App;
