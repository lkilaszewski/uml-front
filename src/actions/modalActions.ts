const openModal = (type, data = null, refetch = null) => ({
  getState,
  setState,
}) => {
  setState({
    ...getState(),
    modal: {
      open: true,
      data,
      refetch,
      type,
    },
  });
};

const closeModal = () => ({ getState, setState }) => {
  setState({
    ...getState(),
    modal: {
      open: false,
      data: null,
      refetch: null,
      type: null,
    },
  });
};

const modalActions = { openModal, closeModal };

export default modalActions;
