const logIn = (history) => () => {
  localStorage.setItem("token", "lorem ipsum");
  history.push("/dashboard");
};

const logOut = (history) => () => {
  localStorage.removeItem("token");
  history.push("/");
};

const authActions = { logIn, logOut };

export default authActions;
