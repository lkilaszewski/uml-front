import React from "react";
import { useStore } from "../../store";
import DeleteDialog from "./DeleteDialog";
import CreateProject from "./CreateProject";
import EditProject from "./EditProject";

const ModalsManager = () => {
  const [state] = useStore();

  switch (state.modal.type) {
    case "delete":
      return <DeleteDialog />;
    case "addProject":
      return <CreateProject />;
    case "editProject":
      return <EditProject />;
    default:
      return <></>;
  }
};

export default ModalsManager;
