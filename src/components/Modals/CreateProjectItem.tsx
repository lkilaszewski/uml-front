import React, { FC } from "react";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import { useMutation, useQueryClient } from "react-query";
import { useFormik } from "formik";
import * as Yup from "yup";
import {
  createStyles,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
} from "@material-ui/core";
import Box from "@material-ui/core/Box";
import { useParams } from "react-router";
import { makeStyles, Theme } from "@material-ui/core/styles";
import axiosInstance from "../../helpers/axios";
import { useStore } from "../../store";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    formControl: {
      width: "100%",
    },
    selectEmpty: {
      marginTop: theme.spacing(2),
    },
  })
);

type Values = {
  name: string;
  project_id: string;
  description: string;
  object_type: string;
  parent_object: number;
};
const ValidationSchema = Yup.object().shape({
  name: Yup.string().required("name is required"),
});

const CreateProjectItem = () => {
  const [state, actions] = useStore();
  const classes = useStyles();
  const queryClient = useQueryClient();
  const { id } = useParams<{ id: string }>();
  const initialValues: Values = {
    name: "",
    project_id: id,
    description: "",
    object_type: "",
    parent_object: null,
  };

  const mutation = useMutation(
    (formData: any) => {
      return axiosInstance().post("/umlv/umlobject/", formData);
    },
    {
      onSuccess: (result) => {
        queryClient.invalidateQueries(`/umlv/treeview/${id}/`);
        actions.closeModal();
      },
    }
  );

  const {
    values,
    errors,
    handleSubmit,
    handleChange,
    touched,
  } = useFormik<Values>({
    initialValues,
    validationSchema: ValidationSchema,
    onSubmit: (vals: any) => {
      mutation.mutate(vals);
    },
  });

  const handleClose = () => {
    actions.closeModal();
  };

  return (
    <div>
      <Dialog
        open={state.modal.open}
        onClose={handleClose}
        aria-labelledby="form-dialog-title"
      >
        <DialogTitle id="form-dialog-title">
          Create new project Object
        </DialogTitle>
        <form onSubmit={handleSubmit}>
          <DialogContent>
            <TextField
              autoFocus
              id="name"
              name="name"
              onChange={handleChange}
              label="Object name"
              type="text"
              fullWidth
            />
            <Box display="none">
              <TextField
                autoFocus
                id="project_id"
                name="project_id"
                onChange={handleChange}
                value={id}
                label="Project Id"
                type="text"
              />
            </Box>
            <FormControl className={classes.formControl}>
              <InputLabel id="object_type">Object Type</InputLabel>
              <Select
                labelId="object_type"
                id="object_type"
                name="object_type"
                value={values.object_type}
                onChange={handleChange}
                fullWidth
              >
                <MenuItem value={"u"}>Przypadek użycia</MenuItem>
                <MenuItem value={"a"}>Aktor</MenuItem>
                <MenuItem value={"f"}>Ramka</MenuItem>
              </Select>
            </FormControl>
            <TextField
              autoFocus
              margin="dense"
              id="parent_object"
              name={"parent_object"}
              label="Object Parent"
              onChange={handleChange}
              type="text"
              fullWidth
            />
            <TextField
              autoFocus
              id="description"
              name="description"
              label="Description"
              type="text"
              multiline
              onChange={handleChange}
              rows={4}
              fullWidth
            />
          </DialogContent>
          <DialogActions>
            <Button onClick={handleClose} color="primary">
              Cancel
            </Button>
            <Button type={"submit"} color="primary">
              Create
            </Button>
          </DialogActions>
        </form>
      </Dialog>
    </div>
  );
};

export default CreateProjectItem;
