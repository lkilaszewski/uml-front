import React, { FC } from "react";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import { useFormik } from "formik";
import * as Yup from "yup";
import { useMutation, useQueryClient } from "react-query";
import axiosInstance from "../../helpers/axios";
import { useStore } from "../../store";

type Values = {
  name: string;
  sh_name: string;
  description: string;
};
const ValidationSchema = Yup.object().shape({
  name: Yup.string().required("name is required"),
});

const initialValues: Values = {
  name: "",
  sh_name: "",
  description: "",
};

const CreateProject = () => {
  const [state, actions] = useStore();
  const queryClient = useQueryClient();
  const mutation = useMutation(
    (formData: any) => {
      return axiosInstance().post("/umlv/project/", formData);
    },
    {
      onSuccess: (result) => {
        queryClient.invalidateQueries(`/umlv/project/`);
        actions.closeModal();
      },
    }
  );
  const {
    values,
    errors,
    handleSubmit,
    handleChange,
    touched,
  } = useFormik<Values>({
    initialValues,
    validationSchema: ValidationSchema,
    onSubmit: (vals: any) => {
      mutation.mutate(vals);
    },
  });
  const handleClose = () => {
    actions.closeModal();
  };

  return (
    <div>
      <Dialog
        open={state.modal.open}
        onClose={handleClose}
        aria-labelledby="form-dialog-title"
      >
        <DialogTitle id="form-dialog-title">Create new project</DialogTitle>
        <form onSubmit={handleSubmit}>
          <DialogContent>
            <TextField
              required
              fullWidth
              id="name"
              onChange={handleChange}
              label="Project name"
              name="name"
              autoFocus
            />
            <TextField
              autoFocus
              margin="dense"
              id={"sh_name"}
              name={"sh_name"}
              onChange={handleChange}
              label="Project Short name"
              type="text"
              fullWidth
            />
            <TextField
              id={"description"}
              name={"description"}
              label="Description"
              multiline
              onChange={handleChange}
              rows={4}
              fullWidth
            />
          </DialogContent>
          <DialogActions>
            <Button onClick={handleClose} color="primary">
              Cancel
            </Button>
            <Button type="submit" color="primary">
              Create
            </Button>
          </DialogActions>
        </form>
      </Dialog>
    </div>
  );
};

export default CreateProject;
