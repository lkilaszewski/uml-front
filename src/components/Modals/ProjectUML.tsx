import React, { FC } from "react";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import { useQuery } from "react-query";
import { createStyles, Grid } from "@material-ui/core";
import { makeStyles, Theme } from "@material-ui/core/styles";
import plantumlEncoder from "plantuml-encoder";
import { useStore } from "../../store";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    dialog: {
      width: "500px",
    },
    selectEmpty: {
      marginTop: theme.spacing(2),
    },
  })
);

const ProjectUML = () => {
  const classes = useStyles();
  const [state, actions] = useStore();
  console.log(state.modal.data);
  const { isLoading, data } = useQuery(`/umlv/plantuml/${state.modal.data}/`);
  const handleClose = () => {
    actions.closeModal();
  };
  if (isLoading) return <p>loading</p>;
  const encoded = plantumlEncoder.encode(data);
  console.log(encoded);
  const url = `http://www.plantuml.com/plantuml/img/${encoded}`;
  return (
    <div>
      <Dialog
        maxWidth={"lg"}
        fullWidth
        open={state.modal.open}
        onClose={handleClose}
        aria-labelledby="max-width-dialog-title"
      >
        <DialogTitle id="form-dialog-title">Project Uml Diagram</DialogTitle>
        <DialogContent dividers>
          <Grid container>
            <Grid item xs={5}>
              <TextField
                autoFocus
                id="description"
                name="description"
                label="Description"
                value={data}
                type="text"
                multiline
                onChange={() => {}}
                rows={12}
                fullWidth
              />
            </Grid>
            <Grid item xs={7}>
              <img src={url} />
            </Grid>
          </Grid>
        </DialogContent>
        <DialogActions>
          <Button autoFocus onClick={handleClose} color="primary">
            Save changes
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
};

export default ProjectUML;
