import React, { FC } from "react";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import { useMutation, useQueryClient } from "react-query";
import { useFormik } from "formik";
import * as Yup from "yup";
import { MenuItem, Select } from "@material-ui/core";
import Box from "@material-ui/core/Box";
import { useParams } from "react-router";
import axiosInstance from "../../helpers/axios";
import { useStore } from "../../store";

type Values = {
  name: string;
  project_id: string;
  description: string;
  object_type: string;
  parent_object: number;
};
const ValidationSchema = Yup.object().shape({
  name: Yup.string().required("name is required"),
});

const EditProjectItem = () => {
  const [state, actions] = useStore();
  const queryClient = useQueryClient();
  const { id } = useParams<{ id: string }>();
  const { data } = state.modal;

  const initialValues: Values = {
    name: data.name,
    project_id: id,
    description: data.description,
    object_type: data.object_type,
    parent_object: data.parent_object,
  };

  const mutation = useMutation(
    (formData: any) => {
      return axiosInstance().put(`/umlv/umlobject/${data.id}/`, formData);
    },
    {
      onSuccess: () => {
        queryClient.invalidateQueries(`/umlv/treeview/${id}/`);
        actions.closeModal();
      },
    }
  );

  const {
    values,
    errors,
    handleSubmit,
    handleChange,
    touched,
  } = useFormik<Values>({
    initialValues,
    validationSchema: ValidationSchema,
    onSubmit: (vals: any) => {
      mutation.mutate(vals);
    },
  });

  const handleClose = () => {
    actions.closeModal();
  };

  return (
    <div>
      <Dialog
        open={state.modal.open}
        onClose={handleClose}
        aria-labelledby="form-dialog-title"
      >
        <DialogTitle id="form-dialog-title">
          Create new project Object
        </DialogTitle>
        <form onSubmit={handleSubmit}>
          <DialogContent>
            <TextField
              autoFocus
              id="name"
              name="name"
              value={values.name}
              onChange={handleChange}
              label="Object name"
              type="text"
              fullWidth
            />
            <Box display="none">
              <TextField
                autoFocus
                id="project_id"
                name="project_id"
                onChange={handleChange}
                value={id}
                label="Object name"
                type="text"
              />
            </Box>
            <Select
              labelId="object_type"
              id="object_type"
              name="object_type"
              value={values.object_type}
              onChange={handleChange}
            >
              <MenuItem value={"u"}>Przypadek użycia</MenuItem>
              <MenuItem value={"a"}>Aktor</MenuItem>
              <MenuItem value={"f"}>Ramka</MenuItem>
            </Select>
            <TextField
              autoFocus
              margin="dense"
              id="parent_object"
              name={"parent_object"}
              value={values.parent_object}
              label="Object Parent"
              onChange={handleChange}
              type="text"
              fullWidth
            />
            <TextField
              autoFocus
              id="description"
              name="description"
              label="Description"
              value={values.description}
              type="text"
              multiline
              onChange={handleChange}
              rows={4}
              fullWidth
            />
          </DialogContent>
          <DialogActions>
            <Button onClick={handleClose} color="primary">
              Cancel
            </Button>
            <Button type={"submit"} color="primary">
              Create
            </Button>
          </DialogActions>
        </form>
      </Dialog>
    </div>
  );
};

export default EditProjectItem;
