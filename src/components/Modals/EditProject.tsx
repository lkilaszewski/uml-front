import React, { FC } from "react";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import { useStore } from "../../store";
import { useFormik } from "formik";
import * as Yup from "yup";
import axiosInstance from "../../helpers/axios";
import { useMutation, useQuery, useQueryClient } from "react-query";
import { Project } from "../../models";

const ValidationSchema = Yup.object().shape({
  name: Yup.string().required("name is required"),
});

const EditProject = () => {
  const [state, actions] = useStore();
  const queryClient = useQueryClient();
  const { data }: { data: Project } = state.modal;
  const mutation = useMutation(
    (formData: any) => {
      return axiosInstance().put(`umlv/project/${data.id}/`, formData);
    },
    {
      onSuccess: (result) => {
        queryClient.invalidateQueries("/umlv/project/");
        actions.closeModal();
      },
    }
  );
  // @ts-ignore
  const initialValues: Project = {
    name: data.name,
    sh_name: data.sh_name,
    description: data.description,
  };

  const {
    values,
    errors,
    handleSubmit,
    handleChange,
    touched,
  } = useFormik<Project>({
    initialValues,
    validationSchema: ValidationSchema,
    onSubmit: (vals: any) => {
      mutation.mutate(vals);
    },
  });
  const handleClose = () => {
    actions.closeModal();
  };

  return (
    <div>
      <Dialog
        open={state.modal.open}
        onClose={handleClose}
        aria-labelledby="form-dialog-title"
      >
        <DialogTitle id="form-dialog-title">Edit project</DialogTitle>
        <form onSubmit={handleSubmit}>
          <DialogContent>
            <TextField
              required
              fullWidth
              id="name"
              onChange={handleChange}
              label="Project name"
              value={values.name}
              name="name"
              autoFocus
            />
            <TextField
              autoFocus
              margin="dense"
              id={"sh_name"}
              name={"sh_name"}
              value={values.sh_name}
              onChange={handleChange}
              label="Project Short name"
              type="text"
              fullWidth
            />
            <TextField
              id={"description"}
              name={"description"}
              label="Description"
              multiline
              value={values.description}
              onChange={handleChange}
              rows={4}
              fullWidth
            />
          </DialogContent>
          <DialogActions>
            <Button onClick={handleClose} color="primary">
              Cancel
            </Button>
            <Button type="submit" color="primary">
              Update
            </Button>
          </DialogActions>
        </form>
      </Dialog>
    </div>
  );
};

export default EditProject;
