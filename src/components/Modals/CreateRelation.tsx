import React, { FC } from "react";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import { useMutation, useQueryClient } from "react-query";
import { useFormik } from "formik";
import * as Yup from "yup";
import {
  createStyles,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
} from "@material-ui/core";
import Box from "@material-ui/core/Box";
import { useParams } from "react-router";
import { makeStyles, Theme } from "@material-ui/core/styles";
import axiosInstance from "../../helpers/axios";
import { useStore } from "../../store";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    formControl: {
      width: "100%",
    },
    selectEmpty: {
      marginTop: theme.spacing(2),
    },
  })
);

type Values = {
  name: string;
  ReLBeg: any;
  ReLEnd: string;
  Stereotype: string;
  RelType: string;
};
const ValidationSchema = Yup.object().shape({
  name: Yup.string().required("name is required"),
});

const CreateRelation = () => {
  const [state, actions] = useStore();
  const classes = useStyles();
  const queryClient = useQueryClient();
  const { data } = state.modal;

  const initialValues: any = {
    name: "",
    RelBeg: data.id,
    Stereotype: "",
    RelEnd: "",
    RelType: "",
  };

  const mutation = useMutation(
    (formData: any) => {
      return axiosInstance().post("/umlv/umlrelation/", formData);
    },
    {
      onSuccess: (result) => {
        queryClient.invalidateQueries(`/umlv/umlrelation/${data.id}/`);
        actions.closeModal();
      },
    }
  );

  const {
    values,
    errors,
    handleSubmit,
    handleChange,
    touched,
  } = useFormik<Values>({
    initialValues,
    validationSchema: ValidationSchema,
    onSubmit: (vals: any) => {
      mutation.mutate(vals);
    },
  });

  const handleClose = () => {
    actions.closeModal();
  };

  return (
    <div>
      <Dialog
        open={state.modal.open}
        onClose={handleClose}
        aria-labelledby="form-dialog-title"
      >
        <DialogTitle id="form-dialog-title">
          Create new project Object
        </DialogTitle>
        <form onSubmit={handleSubmit}>
          <DialogContent>
            <TextField
              autoFocus
              id="name"
              name="name"
              onChange={handleChange}
              label="Object name"
              type="text"
              fullWidth
            />
            <Box display="none">
              <TextField
                autoFocus
                id="RelBeg"
                name="RelBeg"
                onChange={handleChange}
                value={data.id}
                label="Object name"
                type="text"
              />
            </Box>
            <TextField
              autoFocus
              id="RelEnd"
              name="RelEnd"
              onChange={handleChange}
              label="Rel End"
              type="text"
              fullWidth
            />
            <FormControl className={classes.formControl}>
              <InputLabel id="demo-simple-select-label">
                Relation Type
              </InputLabel>
              <Select
                labelId="demo-simple-select-label"
                id="demo-simple-select"
                name="RelType"
                value={values.RelType}
                onChange={handleChange}
                fullWidth
              >
                <MenuItem value={"so"}>Linia ciągła</MenuItem>
                <MenuItem value={"sa"}>Strzałka ciągła</MenuItem>
                <MenuItem value={"da"}>Strzałka przerywana</MenuItem>
              </Select>
            </FormControl>
            <FormControl className={classes.formControl}>
              <InputLabel id="Stereotype">Stereotype</InputLabel>
              <Select
                labelId="Stereotype"
                id="Stereotype"
                name="Stereotype"
                value={values.Stereotype}
                onChange={handleChange}
                fullWidth
              >
                <MenuItem value={"in"}>include</MenuItem>
                <MenuItem value={"ex"}>extend</MenuItem>
                <MenuItem value={"ih"}>inheritance</MenuItem>
              </Select>
            </FormControl>
          </DialogContent>
          <DialogActions>
            <Button onClick={handleClose} color="primary">
              Cancel
            </Button>
            <Button type={"submit"} color="primary">
              Create
            </Button>
          </DialogActions>
        </form>
      </Dialog>
    </div>
  );
};

export default CreateRelation;
