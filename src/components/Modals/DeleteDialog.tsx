import React from "react";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import { useMutation, useQueryClient } from "react-query";
import { useStore } from "../../store";
import axiosInstance from "../../helpers/axios";

const DeleteDialog = () => {
  const [state, actions] = useStore();
  const queryClient = useQueryClient();
  const handleClose = () => {
    actions.closeModal();
  };

  const mutation = useMutation(
    () => {
      return axiosInstance().delete(state.modal.data);
    },
    {
      onSuccess: () => {
        queryClient.invalidateQueries(state.modal.refetch);
        actions.closeModal();
      },
    }
  );

  return (
    <Dialog
      open={state.modal.open}
      onClose={handleClose}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
    >
      <DialogTitle id="alert-dialog-title">{"Are u sure?"}</DialogTitle>
      <DialogContent>
        <DialogContentText id="alert-dialog-description">
          Do you want to delete this item?
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button
          variant="contained"
          color="primary"
          size="small"
          onClick={handleClose}
        >
          Disagree
        </Button>
        <Button
          variant="contained"
          color="secondary"
          size={"small"}
          onClick={() => mutation.mutate()}
          autoFocus
        >
          Agree
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default DeleteDialog;
