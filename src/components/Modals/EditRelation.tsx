import React, { FC } from "react";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import { useMutation, useQueryClient } from "react-query";
import { useFormik } from "formik";
import * as Yup from "yup";
import {
  createStyles,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
} from "@material-ui/core";
import Box from "@material-ui/core/Box";
import { useParams } from "react-router";
import { makeStyles, Theme } from "@material-ui/core/styles";
import axiosInstance from "../../helpers/axios";
import { useStore } from "../../store";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    formControl: {
      width: "100%",
    },
    selectEmpty: {
      marginTop: theme.spacing(2),
    },
  })
);

type Values = {
  name: string;
  rel_beg: string;
  rel_end: string;
  stereotype: string;
  relation_type: string;
};
const ValidationSchema = Yup.object().shape({
  name: Yup.string().required("name is required"),
});

const EditRelation = () => {
  const [state, actions] = useStore();
  const classes = useStyles();
  const queryClient = useQueryClient();
  const { data } = state.modal;
  console.log(data);

  const initialValues: Values = {
    name: data.name,
    rel_beg: data.rel_beg,
    stereotype: data.stereotype,
    rel_end: data.rel_end,
    relation_type: "da",
  };

  const mutation = useMutation(
    (formData: any) => {
      return axiosInstance().put(`/umlv/umlrelation/${data.id}/`, formData);
    },
    {
      onSuccess: () => {
        queryClient.invalidateQueries(`/umlv/umlrelation/${data.rel_beg}/`);
        actions.closeModal();
      },
    }
  );

  const {
    values,
    errors,
    handleSubmit,
    handleChange,
    touched,
  } = useFormik<Values>({
    initialValues,
    validationSchema: ValidationSchema,
    onSubmit: (vals: any) => {
      console.log(vals);
      mutation.mutate(vals);
    },
  });

  const handleClose = () => {
    actions.closeModal();
  };

  return (
    <div>
      <Dialog
        open={state.modal.open}
        onClose={handleClose}
        aria-labelledby="form-dialog-title"
      >
        <DialogTitle id="form-dialog-title">Edit Relation</DialogTitle>
        <form onSubmit={handleSubmit}>
          <DialogContent>
            <TextField
              autoFocus
              id="name"
              name="name"
              value={values.name}
              onChange={handleChange}
              label="Object name"
              type="text"
              fullWidth
            />
            <Box display="none">
              <TextField
                autoFocus
                id="rel_beg"
                name="rel_beg"
                value={values.rel_beg}
                onChange={handleChange}
                label="Object name"
                type="text"
              />
            </Box>
            <TextField
              autoFocus
              id="rel_end"
              name="rel_end"
              value={values.rel_end}
              onChange={handleChange}
              label="Rel End"
              type="text"
              fullWidth
            />
            <FormControl className={classes.formControl}>
              <InputLabel id="relation_type">Relation Type</InputLabel>
              <Select
                labelId="relation_type"
                id="relation_type"
                name="relation_type"
                value={values.relation_type}
                onChange={handleChange}
                fullWidth
              >
                <MenuItem value={"so"}>Linia ciągła</MenuItem>
                <MenuItem value={"sa"}>Strzałka ciągła</MenuItem>
                <MenuItem value={"da"}>Strzałka przerywana</MenuItem>
              </Select>
            </FormControl>
            <FormControl className={classes.formControl}>
              <InputLabel id="stereotype">Stereotype</InputLabel>
              <Select
                labelId="stereotype"
                id="stereotype"
                name="stereotype"
                value={values.stereotype}
                onChange={handleChange}
                fullWidth
              >
                <MenuItem value={"in"}>include</MenuItem>
                <MenuItem value={"ex"}>extend</MenuItem>
                <MenuItem value={"ih"}>inheritance</MenuItem>
              </Select>
            </FormControl>
          </DialogContent>
          <DialogActions>
            <Button onClick={handleClose} color="primary">
              Cancel
            </Button>
            <Button type={"submit"} color="primary">
              Create
            </Button>
          </DialogActions>
        </form>
      </Dialog>
    </div>
  );
};

export default EditRelation;
