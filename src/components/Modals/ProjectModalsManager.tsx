import React from "react";
import { useStore } from "../../store";
import CreateProjectItem from "./CreateProjectItem";
import EditProjectItem from "./EditProjectItem";
import CreateRelation from "./CreateRelation";
import EditRelation from "./EditRelation";
import ProjectUML from "./ProjectUML";

const ProjectModalsManager = () => {
  const [state] = useStore();

  switch (state.modal.type) {
    case "addProjectItem":
      return <CreateProjectItem />;
    case "editProjectItem":
      return <EditProjectItem />;
    case "addRelation":
      return <CreateRelation />;
    case "editRelation":
      return <EditRelation />;
    case "projectUML":
      return <ProjectUML />;
    default:
      return <></>;
  }
};

export default ProjectModalsManager;
