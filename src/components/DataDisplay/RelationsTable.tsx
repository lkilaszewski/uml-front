import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import { useQuery } from "react-query";
import { ButtonGroup, TextField } from "@material-ui/core";
import Button from "@material-ui/core/Button";
import { Delete, Edit, LocationSearching } from "@material-ui/icons";
import { useStore } from "../../store";

const useStyles = makeStyles({
  table: {
    minWidth: 650,
  },
});

function createData(
  name: string,
  calories: number,
  fat: number,
  carbs: number,
  protein: number
) {
  return { name, calories, fat, carbs, protein };
}

const rows = [
  createData("Frozen yoghurt", 159, 6.0, 24, 4.0),
  createData("Ice cream sandwich", 237, 9.0, 37, 4.3),
  createData("Eclair", 262, 16.0, 24, 6.0),
  createData("Cupcake", 305, 3.7, 67, 4.3),
  createData("Gingerbread", 356, 16.0, 49, 3.9),
];

const RelationsTable = ({ activeObject }) => {
  const classes = useStyles();
  const [_, actions] = useStore();
  console.log(activeObject);
  const { isLoading, data }: { isLoading: boolean; data: any } = useQuery(
    `/umlv/umlrelation/${activeObject.id}/`
  );
  if (isLoading) return <p>Loading</p>;
  console.log(data);
  return (
    <TableContainer component={Paper}>
      <Table className={classes.table} aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell>Relation name</TableCell>
            <TableCell align="right">RelEnd</TableCell>
            <TableCell align="right">add_date</TableCell>
            <TableCell align="right">RelType</TableCell>
            <TableCell align="right">Actions</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {data.map((row: any) => (
            <TableRow key={row.id}>
              <TableCell component="th" scope="row">
                {row.name}
              </TableCell>
              <TableCell align="right">{row.rel_end}</TableCell>
              <TableCell align="right">{row.add_date}</TableCell>
              <TableCell align="right">{row.relation_type}</TableCell>
              <TableCell align="right">
                <ButtonGroup
                  variant="contained"
                  color="primary"
                  aria-label="contained primary button group"
                >
                  <Button
                    variant="contained"
                    color="primary"
                    size="small"
                    startIcon={<Edit />}
                    onClick={() => actions.openModal("editRelation", row)}
                  >
                    Edit
                  </Button>
                  <Button
                    variant="contained"
                    color="secondary"
                    size={"small"}
                    startIcon={<Delete />}
                    onClick={() =>
                      actions.openModal(
                        "delete",
                        `/umlv/umlrelation/${row.id}/`,
                        `/umlv/umlrelation/${row.rel_beg}/`
                      )
                    }
                  >
                    Delete
                  </Button>
                </ButtonGroup>
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
};

export default RelationsTable;
