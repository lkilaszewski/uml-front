import React, { FC } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import { ButtonGroup } from "@material-ui/core";
import Button from "@material-ui/core/Button";
import { Delete, Edit, LocationSearching } from "@material-ui/icons";
import { useHistory } from "react-router";
import { useStore } from "../../store";
import { Project } from "../../models";

const useStyles = makeStyles({
  table: {
    minWidth: 650,
  },
});

const DataTable: FC<{ rows: Project[] }> = ({ rows }) => {
  const classes = useStyles();
  const [_, actions] = useStore();
  const history = useHistory();
  return (
    <TableContainer component={Paper}>
      <Table className={classes.table} aria-label="simple table" stickyHeader>
        <TableHead>
          <TableRow>
            <TableCell>Project name</TableCell>
            <TableCell align="right">Owner</TableCell>
            <TableCell align="right">Update Date</TableCell>
            <TableCell align="right">Action</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {rows.map((row) => (
            <TableRow hover key={row.id}>
              <TableCell component="th" scope="row">
                {row.name}
              </TableCell>
              <TableCell align="right">null</TableCell>
              <TableCell align="right">{row.mod_date}</TableCell>
              <TableCell align="right">
                <ButtonGroup
                  variant="contained"
                  color="primary"
                  aria-label="contained primary button group"
                >
                  <Button
                    variant="contained"
                    color="default"
                    size="small"
                    startIcon={<LocationSearching />}
                    onClick={() => {
                      history.push(`/dashboard/project/${row.id}`);
                    }}
                  >
                    Show
                  </Button>
                  <Button
                    variant="contained"
                    color="primary"
                    size="small"
                    startIcon={<Edit />}
                    onClick={() => actions.openModal("editProject", row)}
                  >
                    Edit
                  </Button>
                  <Button
                    variant="contained"
                    color="secondary"
                    size={"small"}
                    startIcon={<Delete />}
                    onClick={() =>
                      actions.openModal(
                        "delete",
                        `umlv/project/${row.id}/`,
                        "/umlv/project/"
                      )
                    }
                  >
                    Delete
                  </Button>
                </ButtonGroup>
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
};

export default DataTable;
