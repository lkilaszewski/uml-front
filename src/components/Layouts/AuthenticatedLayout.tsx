import React, { FC } from "react";
import Container from "@material-ui/core/Container";
import TopBar from "../TopBar";
import ModalsManager from "../Modals/ModalsManager";

interface AuthenticatedLayoutProps {
  children: React.ReactNode;
}

const AuthenticatedLayout: FC<AuthenticatedLayoutProps> = ({ children }) => {
  return (
    <>
      <TopBar />
      <Container maxWidth={"xl"}>{children}</Container>
      <ModalsManager />
    </>
  );
};

export default AuthenticatedLayout;
