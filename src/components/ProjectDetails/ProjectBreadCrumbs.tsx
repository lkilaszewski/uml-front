import { Breadcrumbs } from "@material-ui/core";
import Link from "@material-ui/core/Link";
import Typography from "@material-ui/core/Typography";
import React from "react";
import { useHistory } from "react-router";

const ProjectBreadCrumbs = () => {
  const history = useHistory();
  return (
    <Breadcrumbs aria-label="breadcrumb">
      <Link
        color="inherit"
        href="/dashboard"
        onClick={(e) => {
          e.preventDefault();
          history.push("/dashboard");
        }}
      >
        UmlViewer
      </Link>
      <Typography color="textPrimary">Project</Typography>
    </Breadcrumbs>
  );
};

export default ProjectBreadCrumbs;
