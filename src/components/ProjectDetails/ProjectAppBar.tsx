import React from "react";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import { Add } from "@material-ui/icons";
import Button from "@material-ui/core/Button";
import { useQueryClient } from "react-query";
import { useParams } from "react-router";
import { useStore } from "../../store";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      flexGrow: 1,
    },
    menuButton: {
      marginRight: theme.spacing(2),
    },
    title: {
      flexGrow: 1,
    },
  })
);

const ProjectAppBar = () => {
  const classes = useStyles();
  const [_, actions] = useStore();
  const queryClient = useQueryClient();
  const { id } = useParams<{ id: string }>();
  const data = queryClient.getQueryData(`/umlv/treeview/${id}/`);
  return (
    <AppBar position="static">
      <Toolbar>
        <Typography variant="h6" className={classes.title}>
          {data && data[0].name}
        </Typography>
        <div>
          <Button
            aria-label="account of current user"
            aria-controls="menu-appbar"
            aria-haspopup="true"
            color="inherit"
            startIcon={<Add />}
            onClick={() => actions.openModal("addProjectItem")}
          >
            Add new Item
          </Button>
        </div>
      </Toolbar>
    </AppBar>
  );
};

export default ProjectAppBar;
