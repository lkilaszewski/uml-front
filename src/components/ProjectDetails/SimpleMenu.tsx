import React, { FC } from "react";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import { MoreVert } from "@material-ui/icons";
import IconButton from "@material-ui/core/IconButton";
import { useParams } from "react-router";
import { useStore } from "../../store";

const SimpleMenu: FC<{ item: any }> = ({ item }) => {
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
  const [_, actions] = useStore();
  const { id } = useParams<{ id: string }>();
  console.log(id);
  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    event.preventDefault();
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <>
      <IconButton
        aria-controls="simple-menu"
        aria-haspopup="true"
        onClick={handleClick}
        size={"small"}
      >
        <MoreVert />
      </IconButton>
      <Menu
        id="simple-menu"
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
      >
        <MenuItem
          onClick={() => {
            setAnchorEl(null);
            actions.openModal("editProjectItem", item);
          }}
        >
          Edit
        </MenuItem>
        <MenuItem
          onClick={() => {
            setAnchorEl(null);
            actions.openModal(
              "delete",
              `umlv/umlobject/${item.id}/`,
              `/umlv/treeview/${id}/`
            );
          }}
        >
          Delete
        </MenuItem>
      </Menu>
    </>
  );
};

export default SimpleMenu;
