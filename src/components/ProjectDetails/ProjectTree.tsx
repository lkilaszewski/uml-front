import React, { FC } from "react";
import { makeStyles, Theme, createStyles } from "@material-ui/core/styles";
import TreeView from "@material-ui/lab/TreeView";
import TreeItem, { TreeItemProps } from "@material-ui/lab/TreeItem";
import Typography from "@material-ui/core/Typography";
import Label from "@material-ui/icons/Label";
import FilterFramesIcon from "@material-ui/icons/FilterFrames";
import AccessibilityIcon from "@material-ui/icons/Accessibility";
import ArrowDropDownIcon from "@material-ui/icons/ArrowDropDown";
import ArrowRightIcon from "@material-ui/icons/ArrowRight";
import { SvgIconProps } from "@material-ui/core/SvgIcon";
import SimpleMenu from "./SimpleMenu";

declare module "csstype" {
  interface Properties {
    "--tree-view-color"?: string;
    "--tree-view-bg-color"?: string;
  }
}

type StyledTreeItemProps = TreeItemProps & {
  bgColor?: string;
  color?: string;
  setActiveObject?: (object) => void;
  item?: any;
  labelIcon: React.ElementType<SvgIconProps>;
  labelInfo?: string;
  labelText: string;
  childs?: any;
};

const useTreeItemStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      color: theme.palette.text.secondary,
      "&:hover > $content": {
        backgroundColor: theme.palette.action.hover,
      },
      "&:focus > $content, &$selected > $content": {
        backgroundColor: `var(--tree-view-bg-color, ${theme.palette.grey[400]})`,
        color: "var(--tree-view-color)",
      },
      "&:focus > $content $label, &:hover > $content $label, &$selected > $content $label": {
        backgroundColor: "transparent",
      },
    },
    content: {
      color: theme.palette.text.secondary,
      borderTopRightRadius: theme.spacing(2),
      borderBottomRightRadius: theme.spacing(2),
      paddingRight: theme.spacing(1),
      fontWeight: theme.typography.fontWeightMedium,
      "$expanded > &": {
        fontWeight: theme.typography.fontWeightRegular,
      },
    },
    group: {
      marginLeft: 0,
      "& $content": {
        paddingLeft: theme.spacing(2),
      },
    },
    expanded: {},
    selected: {},
    label: {
      fontWeight: "inherit",
      color: "inherit",
    },
    labelRoot: {
      display: "flex",
      alignItems: "center",
      padding: theme.spacing(0.5, 0),
    },
    labelIcon: {
      marginRight: theme.spacing(1),
    },
    labelText: {
      fontWeight: "inherit",
      flexGrow: 1,
    },
  })
);

const IconManager = (type) => {
  switch (type) {
    case "a":
      return AccessibilityIcon;
    case "f":
      return FilterFramesIcon;
    case "u":
      return Label;
    default:
      return Label;
  }
};

const StyledTreeItem: FC<StyledTreeItemProps> = (props) => {
  const classes = useTreeItemStyles();
  const {
    labelText,
    setActiveObject,
    labelIcon: LabelIcon,
    labelInfo,
    color,
    bgColor,
    childs = null,
    ...other
  } = props;
  return (
    <TreeItem
      onClick={() => setActiveObject(props.item)}
      label={
        <div className={classes.labelRoot}>
          <LabelIcon color="inherit" className={classes.labelIcon} />
          <Typography variant="body2" className={classes.labelText}>
            {labelText}
          </Typography>
          <Typography variant="caption" color="inherit">
            {props.nodeId}
          </Typography>
          <SimpleMenu item={props.item} />
        </div>
      }
      style={{
        "--tree-view-color": color,
        "--tree-view-bg-color": bgColor,
      }}
      classes={{
        root: classes.root,
        content: classes.content,
        expanded: classes.expanded,
        selected: classes.selected,
        group: classes.group,
        label: classes.label,
      }}
      {...other}
    >
      {childs &&
        childs
          .sort((a, b) => a.object_type.localeCompare(b.object_type))
          .map((item) => (
            <StyledTreeItem
              key={item.id}
              nodeId={`${item.id}`}
              labelText={item.name}
              labelIcon={IconManager(item.object_type)}
              item={item}
              setActiveObject={setActiveObject}
              childs={item.child_objects}
            />
          ))}
    </TreeItem>
  );
};

const useStyles = makeStyles(
  createStyles({
    root: {
      height: 264,
      flexGrow: 1,
      maxWidth: 400,
    },
  })
);

const GmailTreeView: FC<{
  data: any;
  setActiveObject: (object) => void;
}> = ({ data, setActiveObject }) => {
  const classes = useStyles();
  return (
    <TreeView
      className={classes.root}
      defaultExpanded={[data[0].id.toString()]}
      defaultSelected={[data[0].id.toString()]}
      defaultCollapseIcon={<ArrowDropDownIcon />}
      defaultExpandIcon={<ArrowRightIcon />}
      defaultEndIcon={<div style={{ width: 24 }} />}
    >
      {data.map((item) => (
        <StyledTreeItem
          key={item.id}
          nodeId={`${item.id}`}
          labelText={item.name}
          labelIcon={IconManager(item.object_type)}
          item={item}
          setActiveObject={setActiveObject}
          childs={item.child_objects}
        />
      ))}
    </TreeView>
  );
};

export default GmailTreeView;
