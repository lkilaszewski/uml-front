import { createStore, createHook, createContainer } from "react-sweet-state";
import modalActions from "../actions/modalActions";
import authActions from "../actions/authActions";

type StoreTypes = {
  token: any;
  modal: {
    open: boolean;
    data: any;
    refetch: string;
    type: string;
  };
};

const initialState: StoreTypes = {
  token: JSON.parse(localStorage.getItem("token")),
  modal: {
    open: false,
    data: null,
    refetch: null,
    type: null,
  },
};

export const Store = createStore({
  initialState,
  actions: { ...modalActions, ...authActions },
  name: "Store",
});

export const useAuth = createHook(Store, {
  selector: null,
});

export const useStore = createHook(Store, {
  selector: (state) => state,
});

export const TokenSubscriber = createContainer(Store, {
  onInit: () => ({ getState, dispatch }) => {},
  onUpdate: () => ({ dispatch }) => {},
  onCleanup: () => ({ setState }) => {
    setState(initialState);
  },
});
