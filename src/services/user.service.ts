import axiosInstance from "../helpers/axios";

const get = (url) => axiosInstance().get(url);
const post = (url, payload) => axiosInstance().post(url, payload);
const patch = (url, payload) => axiosInstance().patch(url, payload);

const getProjects = () => get("umlv/project/");
const getProject = (id) => get(`umlv/project/${id}`);

export { getProjects, getProject };
