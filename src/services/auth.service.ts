import axios from "axios";

export type LoginCredentials = { username: string; password: string };

const login = (data: LoginCredentials) => {
  return axios
    .post(`http://localhost:8000/api/token/`, data)
    .then((response) => {
      localStorage.setItem("token", JSON.stringify(response.data));
      return response.data;
    })
    .catch((error) => {
      throw error;
    });
};

const getCurrentToken = () => JSON.parse(localStorage.getItem("token"));

const refresh = (refreshToken: string) =>
  axios
    .post(`http://localhost:8000/api/token/refresh/`, {
      refresh: refreshToken,
    })
    .then((response) => {
      const newToken = {
        access: response.data.access,
        refresh: getCurrentToken().refresh,
      };
      localStorage.setItem("token", JSON.stringify(newToken));
      return response.data;
    })
    .catch((error) => {
      localStorage.removeItem("token");
      throw error;
    });

const logout = () => {
  localStorage.removeItem("token");
};

const authHeader = () =>
  getCurrentToken() && getCurrentToken().access
    ? { Authorization: `Bearer ${getCurrentToken().access}` }
    : {};

export { authHeader, getCurrentToken, login, logout, refresh };
