import axios, { AxiosInstance } from "axios";
import { authHeader, getCurrentToken, refresh } from "../services/auth.service";

const API_URL = "http://localhost:8000/api";

export default (history = null) => {
  const baseURL = API_URL;
  const axiosInstance: AxiosInstance = axios.create({
    baseURL,
    headers: authHeader(),
  });

  axiosInstance.interceptors.response.use(
    (response) =>
      new Promise((resolve) => {
        resolve(response);
      }),
    (error) => {
      const originalRequest = error.config;
      if (!error.response) {
        return new Promise((resolve, reject) => {
          reject(error);
        });
      }
      if (error.response.status === 403 && originalRequest.retry) {
        localStorage.removeItem("token");
        window.location.href = "/";
        alert("You have been log out, please login again");
        if (history) {
          history.push("/");
        } else {
          window.location.href = "/";
        }
      } else if (error.response.status === 401 && !originalRequest.retry) {
        originalRequest.retry = true;
        return new Promise(function (resolve, reject) {
          refresh(getCurrentToken().refresh)
            .then((response) => {
              originalRequest.headers.Authorization = `Bearer ${response.access}`;
              resolve(axiosInstance.request(originalRequest));
            })
            .catch((err) => {
              localStorage.removeItem("token");
              alert("You have been log out, please login again");
              if (history) {
                history.push("/");
              } else {
                window.location.href = "/";
              }

              reject(err);
            });
        });
      } else {
        return new Promise((resolve, reject) => {
          reject(error);
        });
      }
      return Promise.reject(error);
    }
  );

  return axiosInstance;
};
